﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ControladorDemoKynect : MonoBehaviour
{
    public void cambiarEscena1()
    {
        SceneManager.LoadScene("E1");
    }
    public void CambiarEscena2()
    {
        SceneManager.LoadScene("E2");
    }

    public void CambiarEscena3()
    {
        Application.LoadLevel("e3");
    }

    public void CambiarEscena4()
    {
        SceneManager.LoadScene("Escena2");
    }

    public void CambiarEscena5()
    {
        SceneManager.LoadScene("Escena1");
    }

    public void iniciarKinect()
    {
        KinectManager.IsKinectInitialized();
    }
}
