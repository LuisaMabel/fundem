﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activador_de_objetos : MonoBehaviour
{
  public GameObject[] objects;
  private int currentIndex;
  private float elapsedTime = 0f;
  public float repeatTime = 10f;

  private void Update(){
    elapsedTime += Time.deltaTime;
    if (elapsedTime >= repeatTime)
   {
      int newIndex = Random.Range(0, objects.Length);
      objects[currentIndex].SetActive(false);
      currentIndex = newIndex;
      objects[currentIndex].SetActive(true);
      elapsedTime -= repeatTime;
     }
   }
 }

